variable "base-name" {
  description = "the base name for the resources"
  default = "nivaAi"
}

# variable "base-env" {
#   description = "deployment environment"
#   default = "dev"
# }

variable "linux-image" {
  # default = "Ubuntu-22.04-LTS"
  default = "Ubuntu-20.04"
}

variable "master-volume-size" {
  default = 20
}

variable "compute-volume-size" {
  default = 20
}

variable "data-volume-size" {
  default = 50
}
