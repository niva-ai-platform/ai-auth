locals {
  resource-base = "${var.base-name}-${var.base-env}"
}

resource "openstack_compute_instance_v2" "kniva-rp" {
  name            = "${local.resource-base}"
  image_name      = "${var.image}"
  flavor_name     = "${var.flavor}"
  key_pair        = "${var.ssh-remote-key}"
  security_groups = ["default", "wfh"]

  network {
    name = "${var.network}"
  }
}

resource null_resource "upload-file" {
  triggers = {
    instance_id = "${openstack_compute_instance_v2.kniva-rp.id}"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    # private_key = "${openstack_compute_instance_v2.kniva-rp.key_pair}"
    private_key = "${file("${var.ssh-local-key}")}"
    host        = "${openstack_compute_instance_v2.kniva-rp.access_ip_v4}"
    agent       = false
  }

  provisioner "file" {
    source = "./docker-setup.sh"
    destination = "/home/ubuntu/docker-setup.sh"
  }

  provisioner "file" {
    source = "./traefik_dynamic.yml"
    destination = "/home/ubuntu/traefik/traefik_dynamic.yml"
  }

  provisioner "file" {
    source = "./docker-compose.yaml"
    destination = "/home/ubuntu/traefik/docker-compse.yaml"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x docker-setup.sh",
      "cd traefik && touch acme.json && chmod 0600 acme.json"
    ]
  }
}

# Output VM IP Address
output "serverip" {
  value = openstack_compute_instance_v2.kniva-rp.access_ip_v4
}

#----------------------------
