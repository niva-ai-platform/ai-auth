variable "base-name" {
  description = "the base name for the resources"
  default = "nivaAi"
}

variable "base-env" {
  description = "deployment environment"
  default = "rp-dev-1"
}

variable "image" {
  default = "Ubuntu-22.04-LTS"
}

variable "flavor" {
  default = "W2.2C-2G-20"
}

variable "network" {
  default = "Walton-ext-dual-net"
}

variable "ssh-remote-key" {
  default = "ktest-master-key"
}

variable "ssh-local-key" {
  default = "~/.ssh/ktest-master-key"
}
