#! /bin/bash

sshKey=~/.ssh/ktest-master-key
deployServer=ubuntu@niva-ai-dev.tssg.org
dest=./traefik

scp -i $sshKey docker-compose.yaml $deployServer:$dest/.
scp -i $sshKey traefik_dynamic.yml $deployServer:$dest/.
#scp -i $sshKey init-letsencrypt.sh $deployServer:$dest/.
#scp -i $sshKey nginx.conf $deployServer:$dest/data/nginx/.
