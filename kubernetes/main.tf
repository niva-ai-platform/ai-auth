locals {
  # resource-base = "${var.base-name}-${var.base-env}"
    resource-base = "${var.base-name}-${terraform.workspace}"
}

resource "openstack_compute_instance_v2" "master" {
  name            = "${local.resource-base}-master"
  image_name      = "${var.linux-image}"
  flavor_name     = "GL.4C-4G-20"
  key_pair        = "ktest-master-key"
  security_groups = ["default", "wfh", "ktest", "ktest-node"]
  # user_data = "#!/bin/bash\n sudo gitlab-runner verify"

  network {
    name = "Walton-ext-dual-net"
  }
}

resource null_resource "upload-file" {
  triggers = {
    instance_id = "${openstack_compute_instance_v2.master.id}"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${file("~/.ssh/ktest-master-key")}"
    host        = "${openstack_compute_instance_v2.master.access_ip_v4}"
    agent       = false
  }

  # provisioner "file" {
  #   source = "~/.ssh/msu.tssg.org.key"
  #   destination = "/home/ubuntu/.ssh/msu.key"
  # }

  # provisioner "file" {
  #   source = "./docker-setup.sh"
  #   destination = "/home/ubuntu/docker-setup.sh"
  # }

  provisioner "file" {
    source = "./kb8-setup.sh"
    destination = "/home/ubuntu/kb8-setup.sh"
  }

  provisioner "remote-exec" {
    inline = [
#      "chmod +x docker-setup.sh",
      "chmod +x kb8-setup.sh",
#      "sudo ./docker-setup.sh"
    ]
  }
}

# resource "openstack_blockstorage_volume_v2" "master-volume" {
#   name = "${local.resource-base}-kmaster-volume"
#   size = var.master-volume-size
# }

# resource "openstack_compute_volume_attach_v2" "vmaster" {
#   instance_id = openstack_compute_instance_v2.master.id
#   volume_id   = openstack_blockstorage_volume_v2.master-volume.id
# }

# Output VM IP Address
output "serverip" {
  value = openstack_compute_instance_v2.master.access_ip_v4
}

# output "volume" {
#   value = openstack_compute_volume_attach_v2.vmaster.id
# }

#----------------------------

resource "openstack_compute_instance_v2" "compute-node" {
  count = 3
  name = "${local.resource-base}-compute-node-${count.index}"
  image_name      = "${var.linux-image}"
  flavor_name     = "GL.4C-4G-20"
  key_pair        = "ktest-master-key"
  security_groups = ["default", "wfh", "ktest-node"]
  # user_data = "#!/bin/bash\n sudo gitlab-runner verify"

  network {
    name = "Walton-ext-dual-net"
  }
}

# resource "openstack_blockstorage_volume_v2" "compute-node-volume" {
#   count = 3
#   name = "${local.resource-base}-node-volume-${count.index}"
#   size = var.compute-volume-size
# }

# resource "openstack_compute_volume_attach_v2" "vnode" {
#   count = 3
#   instance_id = openstack_compute_instance_v2.compute-node[count.index].id
#   volume_id   = openstack_blockstorage_volume_v2.compute-node-volume[count.index].id
# }

# kubeadm join 87.44.17.162:6443 --token yn180i.sqfdgawjbjk2kd1k \
#         --discovery-token-ca-cert-hash sha256:e96120e17f1a598859a7dad0a8651b0708602964a3488c8cd149c72fd5dceda7


# resource null_resource "upload-file-node" {
#   triggers = {
#     instance_ids = "${openstack_compute_instance_v2.compute-node.*.id}"
#   }

#   connection {
#     type        = "ssh"
#     user        = "ubuntu"
#     private_key = "${file("~/.ssh/ktest-master-key")}"
#     host        = "${openstack_compute_instance_v2.compute-node.*.access_ip_v4}"
# #    host        = element(openstack_compute_instance_v2.compute-node.*.access_ip_v4, 0)
# #    host        = "${openstack_compute_instance_v2.compute-node[0].access_ip_v4}"
#     agent       = false
#   }

#   # provisioner "file" {
#   #   source = "~/.ssh/msu.tssg.org.key"
#   #   destination = "/home/ubuntu/.ssh/msu.key"
#   # }

#   # provisioner "file" {
#   #   source = "./docker-setup.sh"
#   #   destination = "/home/ubuntu/docker-setup.sh"
#   # }

#   provisioner "file" {
#     source = "./kb8-setup.sh"
#     destination = "/home/ubuntu/kb8-setup.sh"
#   }

#   provisioner "remote-exec" {
#     inline = [
# #      "chmod +x docker-setup.sh",
#       "chmod +x kb8-setup.sh",
# #      "sudo ./docker-setup.sh"
#     ]
#   }
# }


output "nodeip" {
  value = openstack_compute_instance_v2.compute-node.*.access_ip_v4
}

# output "nodevolume" {
#   value = openstack_compute_volume_attach_v2.vnode.*.id
# }

# #----------------------------

# resource "openstack_compute_instance_v2" "data-node" {
#   name            = "${local.resource-base}-node-data"
#   image_name      = "Ubuntu-20.04"
#   flavor_name     = "GL.4C-4G-20"
#   key_pair        = "ktest-master-key"
#   security_groups = ["default", "wfh"]

#   network {
#     name = "Walton-ext-dual-net"
#   }
# }

# resource "openstack_blockstorage_volume_v2" "data-node-volume" {
#   name = "${local.resource-base}-node-data-volume"
#   size = var.data-volume-size
# }

# resource "openstack_compute_volume_attach_v2" "vdatanode" {
#   instance_id = openstack_compute_instance_v2.data-node.id
#   volume_id   = openstack_blockstorage_volume_v2.data-node-volume.id
# }

# # Output VM IP Address
# output "data-node-ip" {
#   value = openstack_compute_instance_v2.data-node.access_ip_v4
# }

# output "data-node-volume" {
#   value = openstack_compute_volume_attach_v2.vdatanode.id
# }


# # resource "local_file" "after-script" {
# #   filename = "${path.module}/after-script.sh"
# #   content = "ssh ${openstack_compute_instance_v2.master.access_ip_v4}\n" + "${var.nodeip.value}"
# # }
