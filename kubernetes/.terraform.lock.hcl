# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/null" {
  version = "3.1.1"
  hashes = [
    "h1:71sNUDvmiJcijsvfXpiLCz0lXIBSsEJjMxljt7hxMhw=",
    "h1:Pctug/s/2Hg5FJqjYcTM0kPyx3AoYK1MpRWO0T9V2ns=",
    "zh:063466f41f1d9fd0dd93722840c1314f046d8760b1812fa67c34de0afcba5597",
    "zh:08c058e367de6debdad35fc24d97131c7cf75103baec8279aba3506a08b53faf",
    "zh:73ce6dff935150d6ddc6ac4a10071e02647d10175c173cfe5dca81f3d13d8afe",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:8fdd792a626413502e68c195f2097352bdc6a0df694f7df350ed784741eb587e",
    "zh:976bbaf268cb497400fd5b3c774d218f3933271864345f18deebe4dcbfcd6afa",
    "zh:b21b78ca581f98f4cdb7a366b03ae9db23a73dfa7df12c533d7c19b68e9e72e5",
    "zh:b7fc0c1615dbdb1d6fd4abb9c7dc7da286631f7ca2299fb9cd4664258ccfbff4",
    "zh:d1efc942b2c44345e0c29bc976594cb7278c38cfb8897b344669eafbc3cddf46",
    "zh:e356c245b3cd9d4789bab010893566acace682d7db877e52d40fc4ca34a50924",
    "zh:ea98802ba92fcfa8cf12cbce2e9e7ebe999afbf8ed47fa45fc847a098d89468b",
    "zh:eff8872458806499889f6927b5d954560f3d74bf20b6043409edf94d26cd906f",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.47.0"
  constraints = "~> 1.47.0"
  hashes = [
    "h1:6GKN5WfUZVVJ9cBHwN74RVOzMK2xlZx1qwGo8CUtPPk=",
    "h1:86uOFrr8BUK1SIoWS51Ip7w/x8WcoHyrXGEZ576iNSM=",
    "zh:110dfeb02d47af0d1c71d41deac7bb0b23d0d5f9f2ac95f81fee7be1b7131852",
    "zh:164c141ca8d1d1b43b866150797f5e15855d48aaddd50a80ca320e638cdbbd3a",
    "zh:1be4fab5de93f2947c35df6676c67bf2ea410ec71f29e4a57661119c2f262d6a",
    "zh:40dc8f1ffc3521786b38427fe2c0f2ec0b102fed4284c08b2d092ebd1617a603",
    "zh:6335190faaeba5bd5a859df7075c6a820a1f492d1f2258296a4fb3170b4f0643",
    "zh:6460c8c651ba96d434e0db0e09cb02b6188861a8a683b1f9a488d0e119deeb71",
    "zh:6ba87e36384c8c165e0853366252451a20a2c1aeec1a2280eb208db200b5ce33",
    "zh:6ebbb699c8673ebbaa5fc880c7b3a8dcf69d51bf242c72b986cf6b919bac4969",
    "zh:719dc962699d17d03f017d27809c68162a2a849e478e04b858cd798be737a26a",
    "zh:7d865928a695dc3558f671da12dbb134d822968d2f0cb4b600ce79c41a0d1d4b",
    "zh:b5c50a6ab05cb18780f4d4b4d48f502d60f4121810127105f5f7d34d2dda9569",
    "zh:df3adcaacfdd3696a9df768beb3ec997410724c5dbd1cc5fd023e6644686bd74",
    "zh:f836c285e5e36ddaa3d0d13f4e89fcb9047356cfb09c77651cdf97b6394af7fa",
    "zh:f91081a4f26b69b8325149353d3199838466e971853a2d1e9c2917051a6fdd6d",
  ]
}
