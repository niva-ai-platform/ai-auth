# !/bin/bash

## setup disk
## takes a uuid as the drive to setup

# volume=$1

# printf '%s\n' n '' '' '' '' w | fdisk /dev/$volume

# mkfs.ext4 /dev/$volume
# printf '\nPartition formatted\n\n'

# # # ceate mount point
# mkdir /mnt/docker

# # # add mount point to fstab file
# newLine=$(echo "/dev/${volume}1 /mnt/docker ext4 defaults 0 0")
# echo $newLine | sudo tee -a /etc/fstab

# # test new fstab setting, test drive is mounted:
# mount -a


## download, install and configure docker

curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh

cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

apt update

sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt update
sudo apt install -y kubelet kubeadm kubectl nfs-common
sudo apt-mark hold kubelet kubeadm kubectl

apt upgrade -y

# reboot nowexi
