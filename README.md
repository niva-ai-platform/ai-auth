# __template_project__

To be used as a template project when creating other projects

## To Do

- Create a .gitignore appropiate you your project/code type
- Install a editor config plugin for you editor
- Verify the .editorconfig file has the correct setting for your file types
  - We default across all projects to space for tab, spaces: 2, <LF> line endings
- Add a simple build test to confirms there are no code errors
