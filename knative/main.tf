locals {
  resource-base = "${var.base-name}-${var.base-env}"
}

resource "openstack_compute_instance_v2" "knative-test" {
  name            = "${local.resource-base}"
  image_name      = "Ubuntu-20.04"
  flavor_name     = "W5.4C-8G-20"
  key_pair        = "ktest-master-key"
  security_groups = ["default", "wfh", "ktest"]

  network {
    name = "Walton-ext-dual-net"
  }
}

resource null_resource "upload-file" {
  triggers = {
    instance_id = "${openstack_compute_instance_v2.knative-test.id}"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${file("~/.ssh/ktest-master-key")}"
    host        = "${openstack_compute_instance_v2.knative-test.access_ip_v4}"
    agent       = false
  }

  provisioner "file" {
    source = "./knative-setup.sh"
    destination = "/home/ubuntu/knative-setup.sh"
  }

  provisioner "remote-exec" {
    inline = [
#      "chmod +x docker-setup.sh",
      "chmod +x knative-setup.sh",
#      "sudo ./docker-setup.sh"
    ]
  }
}

# Output VM IP Address
output "serverip" {
  value = openstack_compute_instance_v2.knative-test.access_ip_v4
}

#----------------------------
