terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.47.0"
    }
  }
}

provider "openstack" {
  auth_url = "http://cloud.waltoninstitute.ie:5000/v3/"
  tenant_id = "013c5e68f38741e59f403331ad906391"
  tenant_name = "IPS"
  user_domain_name = "Walton"
  project_domain_id = "827a2d41d5064218b57baa65ed4b9413"
  region = "cloudTwentyOne"
  # interface = "public"
  # identity_api_version=3
}
